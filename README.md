# README #

Starter for golang workspace with go modules support

Based on https://go.dev/doc/tutorial/call-module-code

## Modules Support outside of GOPATH
1. Add a go.mod to the package
2. Run 'go mod edit -replace example.com/greetings=../greetings' inorder to make the module available for the importing module